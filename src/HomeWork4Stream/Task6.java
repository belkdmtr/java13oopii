package HomeWork4Stream;

/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
 */

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {

        //СОЗДАДИМ КОЛЛЕКЦИЮ из SETов
        Set<Set<Integer>> twoStetInteger = new HashSet<>();

        //первый Set
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);

        //второй set
        Set<Integer> set2 = new HashSet<>();
        set2.add(3);
        set2.add(4);

        //Добавим два созданных списка в основную коллекуцию twoStetInteger
        twoStetInteger.add(set1);
        twoStetInteger.add(set2);

        //Предварительный вывод результатов
        System.out.println("Массив сетов " + twoStetInteger);

        //При помощи стрима создадим плоскую коллекцию SET из двух SETов при помощи .flatMap
        Set<Integer> flatSet = twoStetInteger.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

        //Результат работы
        System.out.println("Плоский масссив состоящий из двух сетов " + flatSet);

    }
}
