package HomeWork4Stream;

/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) {

        //Создадим коллекцию чисел от 1 до 5
        List<Integer> myListDigit = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            myListDigit.add(i);
        }


        //Создадим переменную sum и при помощи потока запишем туда значение
        Integer multiplier = myListDigit.stream()
                .reduce(1, (a, b) -> a * b); //терминальная операция, сокращение элементов потока (аккумулятор)

        System.out.println(multiplier);



    }
}
