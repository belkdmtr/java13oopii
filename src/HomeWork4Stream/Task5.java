package HomeWork4Stream;

/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {

        //Создадим список строк
        List<String> myListString = new ArrayList<>();
        myListString.add("abc");
        myListString.add("Dvb");
        myListString.add("stR");
        myListString.add("def");
        myListString.add("qqq");

        String str = myListString.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));

        System.out.println(str + ".");

    }
}
