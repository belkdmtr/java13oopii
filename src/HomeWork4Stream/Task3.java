package HomeWork4Stream;

/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Task3 {
    public static void main(String[] args) {

        //Создадим список строк
        List<String> myListString = new ArrayList<>();
        myListString.add("abc");
        myListString.add("");
        myListString.add("");
        myListString.add("def");
        myListString.add("qqq");

        //так как .count возращает long посчитаем строки удовлетворяющие условию
        long count = myListString.stream()
                .filter(str -> !Objects.equals(str, ""))
                .count();

        System.out.println(count);

    }
}
