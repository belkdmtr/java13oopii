package HomeWork4Stream;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false

 */

import java.util.function.BiPredicate;

public class Task7JobInterview {
    public static void main(String[] args) {


        BiPredicate<String, String> checkWords = (String word1, String word2) -> {
            int count = 0;

            //1. Провим можно ли добавить символ (не более 1го)
            //если символов больше 2х то возращаем false
            if (word2.length() > word1.length() + 1) {
                return false;
            }

            //2. Счетчик содержащий колчисетсво измененных символов (по улсовию их не более 1-го может быть)
            for (int i = 0; i < word1.length(); i++) {
                if (word1.charAt(i) != word2.charAt(i)) {
                    count++;
                }
            }

            //3. Проверка условия.
            //Если в слове изменено более 2х букв, то false
            if (count >= 2) {
                return false;
            } else {
                return true;
            }
        };

        //На вход подается две строки согласно условию задачи
        System.out.println(checkWords.test("cat", "cats"));
        System.out.println(checkWords.test("cat", "cut"));
        System.out.println(checkWords.test("cat", "nut"));
    }
}
