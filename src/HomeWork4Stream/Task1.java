package HomeWork4Stream;

import java.util.ArrayList;
import java.util.List;

/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */


public class Task1 {
    public static void main(String[] args) {

        //Создадим коллекцию чисел от 1 до 100
        List<Integer> myListDigit = new ArrayList<>();

        for (int i = 1; i <= 100; i++) {
            myListDigit.add(i);
        }

        //Создадим переменную sum и при помощи потока запишем туда значение
        Integer sum = myListDigit.stream()
                .filter(digit -> digit % 2 == 0)
                .reduce(0, Integer::sum); //терминальная операция, сокращение элементов потока (аккумулятор)

        System.out.println(sum);
    }
}
