package HomeWork4Stream;

/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Task4 {
    public static void main(String[] args) {

        //Создадим коллекцию вещественных чисел
        List<Double> myListDigit = new ArrayList<>();
        myListDigit.add(30.0);
        myListDigit.add(1.0);
        myListDigit.add(1.5);
        myListDigit.add(17.0);
        myListDigit.add(10.0);
        myListDigit.add(22.0);

        //Создание компоратора для сортировки значений в обратном порядке
        myListDigit.stream()
               .sorted((o1, o2) -> {
                   if (Objects.equals(o1, o2)) {
                       return 0;
                   }
                   if (o1 < o2) {
                       return 1;
                   } else {
                       return -1;
                   }
               })
               .forEach(System.out::println);
    }
}
