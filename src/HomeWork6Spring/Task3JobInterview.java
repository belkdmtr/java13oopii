package HomeWork6Spring;

/*
Напишите программы, чтобы узнать, является ли введенное число простым или нет.
 */

import java.util.Scanner;

public class Task3JobInterview {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int num = scanner.nextInt();

        System.out.println(checkNumber(num));

    }

    public static boolean checkNumber(int num) {
        for (int i = 2; i <= Math.sqrt(num); i++) {
            System.out.println(i);
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}

/*
2	3	5	7	11	13	17	19	23	29	31	37	41	43	47
 */