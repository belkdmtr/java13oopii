package HomeWork6Spring;

/*
Напишите программу для проверки, является ли введение число - числом Армстронга.

Число Армстронга — натуральное число, которое в данной системе счисления равно сумме
своих цифр, возведенных в степень, равную количеству его цифр. Иногда, чтобы считать
число таковым, достаточно, чтобы степени, в которые возводятся цифры, были равны m.

Например, десятичное число
153— число Армстронга, потому что
1^3 + 5^3 + 3^3 = 153.
 */

import java.util.Scanner;

public class Task2JobInterview {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число: ");

        int num = scanner.nextInt();
        final int number = num;
        int result = 0;

        // Найдем длину нашего числа, при помощи Math.log10
        int length = (int) (Math.log10(num) + 1);


        for (int i = 0; i < length; i++) {
            result += Math.pow((num % 10), length); //возведем в степерь последннее число в цифре
            num = num / 10; //разделим наше число на 10, что бы получить следующую цифру
        }

        //Произведем проверку на уовлетворение условию
        System.out.println(number == result);

    }
}
