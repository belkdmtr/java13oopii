package HomeWork1.Task3;

/**
 * Реализовать метод, открывающий файл ./input.txt
 * и сохраняющий в файл /output.txt текст из input,
 * где каждый латинский строчный символ заменен на
 * соответствующий заглавный. Обязательно использование try с ресурсами.
 */

public class Main {
    public static void main(String[] args) {

        ReadWriteFile readWriteFile = new ReadWriteFile();

        try {
            readWriteFile.setInputFile("./src/HomeWork1/Task3/Files/input.txt");
            readWriteFile.setOutputFile("./src/HomeWork1/Task3/Files/output.txt");
            readWriteFile.goReadWriteFile();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }





    }
}
