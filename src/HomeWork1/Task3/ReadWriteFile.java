package HomeWork1.Task3;

import java.io.*;

public class ReadWriteFile {

    private String inputFile;
    private String outputFile;

    //private String file;

    public ReadWriteFile() {
    }

    public void setInputFile(String inputFile) throws Exception {
        //Проверка, что файл существует
        File file = new File(inputFile);
        if (!file.exists()) {
            throw new Exception("Файл для чтения не найден");
        }
        this.inputFile = inputFile;
    }

    public void setOutputFile(String outputFile) throws Exception {
        //Проверка, что файл существует
        File file = new File(outputFile);
        if (!file.exists()) {
            throw new Exception("Файл для записи не найден");
        }
        this.outputFile = outputFile;
    }

    public void goReadWriteFile() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(inputFile)); // Файл для чтения в reader
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile)); // Файл для чтения в writer

        // Считал первую строку
        String line = reader.readLine();

        //Проверка, пустой ли файл?
        if (line == null) {
            throw new IOException("Файл для чтения пустой!");
        }

        // Для чистоты эксперемента очситим файл в который будем писать
        writer.flush();

        // Если строка не пустая (не вернула -1) я начинаю в цикле выводить строки
        while (line != null) {
            writer.write(line.toUpperCase());
            line = reader.readLine();
            if (line != null) {
                writer.write("\n");
            }
        }
        reader.close();
        writer.close();
    }
}
