package HomeWork1.Task1;

public class Main {
    public static void main(String[] args) {

        YouAge youAge = new YouAge();

        try {
            // 1. Создать собственное исключение MyCheckedException, являющееся проверяемым.
            youAge.setAge(30); //Правильный возраст
            System.out.println("Дата рождения установлена " + youAge.getAge());
            youAge.setAge(101); //Ошибочный возраст

        } catch (MyCheckedException e) {
            System.err.println(e.getMessage());
        }


    }
}
