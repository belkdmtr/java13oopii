package HomeWork1.Task1;

public class MyCheckedException extends Exception {

    //Переопределим все конструкторы своего Exception,
    // ALT + ISERT добавим их!!!!!!!!!!!!!!!!!!!!!!!!!!!


    //Пустой конструктор
    protected MyCheckedException() {
    }

    //Констурктор с сообщением
    protected MyCheckedException(int age) {
        super("Не корректный возраст " + age);
    }


/*
    //Констурктор с сообщением и причиной
    public MyCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    //Констурктор с причиной ошибки
    public MyCheckedException(Throwable cause) {
        super(cause);
    }

    public MyCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    //Переопределение объекта toString
    @Override
    public String toString() {
        return "Делить на цифру " + year + " нельзя!";
    }*/
}
