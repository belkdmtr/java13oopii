package HomeWork1.Task1;

public class YouAge {

    private int age;

    protected YouAge() {
    }

    protected int getAge() {
        return age;
    }

    //MyCheckedException является проверяемым исключением,
    // оно должно быть объявлено в заголовке метода
    protected void setAge(int age) throws MyCheckedException { //Обработка сигнатуры ошибки в методе
        if (age < 0 || age > 100) {
            //Если возникает ошибка, мы БРОСАЕМ пользователю ошибку
            throw new MyCheckedException(age);
        }
        this.age = age;
    }
}
