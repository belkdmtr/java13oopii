package HomeWork1.Task5;

// 5. Найти и исправить ошибки в следующем коде
// (сдать исправленный вариант):

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Добавил ловушку для Exception из try / catch вылетающую из метода inputN()
        try {
            int n = inputN();
            System.out.println("Успешный ввод!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // ******* МЕТОД для предварительной обработки введеного значения *******
    private static int inputN() throws Exception { // Добавил обработку сигнатуры ошибки в методе
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 100 || n < 0) { // Исправил условие if с (n < 100 && n > 0),
                                // что бы ошибка выбрасывалась при невыполнении условия 0 < n < 100
            throw new Exception("Неверный ввод");
        }
        return n;
    }
}
