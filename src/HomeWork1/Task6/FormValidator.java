package HomeWork1.Task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

public class FormValidator {

    private String str;

    //Безаргументный контруктор для статического класса
    protected FormValidator() {
    }

    // Паттерн "Имя" удовлетворяющий условиям:
    // - длина имени должна быть от 2 до 20 символов, первая буква заглавная.
    private static final Pattern NAME_PATTERN = Pattern.compile("(^[A-Z]{1}[a-z]{1,19})");

    // Паттерн "Дата рождения"
    private static final Pattern DATE_PATTERN = Pattern.compile("[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}");

    // **** A) Длина имени должна быть от 2 до 20 символов, первая буква заглавная. ****
    public static void checkName(String str)
            throws Exception {
        if (!NAME_PATTERN.matcher(str).matches()) {
            throw new Exception("ОШИБКА! Длина имени должна быть от 2 до 20 символов, первая буква заглавная");
        } else {
            System.out.println("Имя " + str);
        }
    }


    // **** B) Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты. ****
    public static void checkBirthdate(String str) throws Exception {
        //Проверка на корректный вод формата даты рождения
        if (!DATE_PATTERN.matcher(str).matches()) {
            throw new Exception("ОШИБКА! Не верный формат даты");
        }
        // Задаем формат даты рождения.
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date beforeDate = dateFormat.parse("01.01.1900"); // Переменная хранящая минимальную дату
        Date nowDate = new Date();                               // Переменная хранящая дату рождения
        Date dateOfBirth = dateFormat.parse(str);                // Переменная харнящая текущую дату.

        //Выбрасывает ошибку если дата рождения вне диапазона
        if (!beforeDate.before(dateOfBirth) || !nowDate.after(dateOfBirth)) {
           throw new Exception("ОШИБКА! Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
        } else {
            System.out.println("Дата рождения " + dateFormat.format(dateOfBirth));
        }
    }

    // **** С) Пол должен корректно матчится в enum Gender, хранящий Male и Female значения ****
    public static void checkGender(String str) throws Exception {
        //***************************************
        //Я не знаю как это работает, почему оператор И работает как ИЛИ???
        //***************************************
        if (!Gender.MALE.myEqualValue(str) && !Gender.FEMALE.myEqualValue(str)) {
            throw new Exception("ОШИБКА! Не верно указан пол");
        } else {
            System.out.println("Пол " + str.toUpperCase());
        }
    }

    // **** D) Рост должен быть положительным числом и корректно конвертироваться в double. ****
    public static void checkHeight(String str) throws Exception{

        if (Double.valueOf(str) < 0 ) {
            throw new Exception(" ОШИБКА! Рост должен быть положительным");
        } else {
            System.out.println("Рост " + Double.valueOf(str));
        }

    }

}
