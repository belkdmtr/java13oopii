package HomeWork1.Task6;

/**
 * Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
 * очень зол и ему придется написать свои проверки, а также кидать исключения,
 * если проверка провалилась. Помогите Пете написать класс FormValidator со
 * статическими методами проверки. На вход всем методам подается String str.
 *
 * a. public void checkName(String str) — длина имени должна быть от 2 до 20
 * символов, первая буква заглавная.
 *
 * b. public void checkBirthdate(String str) — дата рождения должна быть не
 * раньше 01.01.1900 и не позже текущей даты.
 *
 * c. public void checkGender(String str) — пол должен корректно матчится в
 * enum Gender, хранящий Male и Female значения.
 *
 * d. public void checkHeight(String str) — рост должен быть положительным
 * числом и корректно конвертироваться в double.
 */

public class Main {
    public static void main(String[] args) {


        // 1) В данном кейсе все заполнено ьез ошибок
        try {
            FormValidator.checkName("Dmitrii");             // Проверяем Имя
            FormValidator.checkBirthdate("06.07.1983");     // Проверяем дату рождения
            FormValidator.checkGender("MALe");              // Проверка пола
            FormValidator.checkHeight("183");               // Проверка роста
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println();
        // 2) В текущем кейсе, есть ошибка при вводе пола
        try {
            FormValidator.checkName("Alexa");            // Проверяем Имя
            FormValidator.checkBirthdate("10.12.1994");  // Проверяем дату рождения
            FormValidator.checkGender("femaleW");        // Здесь перехват ошибки при проверке пола
            FormValidator.checkHeight("183");            // ---------
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
