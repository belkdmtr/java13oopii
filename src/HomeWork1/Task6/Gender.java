package HomeWork1.Task6;

public enum Gender {
    //В данном случае сравнение идет по Male и Female
    MALE ("MALE"),
    FEMALE ("FEMALE");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    //Выдает тру если введен верно пол
    public boolean myEqualValue (String value) {
        return this.value.equalsIgnoreCase(value);
    }
}