package HomeWork1.Task2;

//Создать собственное исключение MyUncheckedException, являющееся непроверяемым.

public class Main {
    public static void main(String[] args) throws MyUncheckedException {

        try {
            //Строка без ошибки превращается в число
            double str1 = Double.parseDouble("100");
            System.out.println("Число str1 = " + str1);

            //Данную строку нельзя перевести в число, поэтому перехватываем ошибку.
            double str2 = Double.parseDouble("qwe");
            System.out.println("Число str2 = " + str2);
        // непроверяемое исключение NumberFormatException
        // когда оно не может отформатировать (преобразовать) строку в число
        } catch (NumberFormatException e) {
            throw new MyUncheckedException();
        } finally {
            System.out.println("Конец программы");
        }
    }
}
