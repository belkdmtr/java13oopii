package HomeWork1.Task2;

public class MyUncheckedException extends Exception {

    protected MyUncheckedException() {
        super("Не верный формат строки!");
    }

    public MyUncheckedException(String message, Throwable cause) {
        super(message, cause);
    }
}
