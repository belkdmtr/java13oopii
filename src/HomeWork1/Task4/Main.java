package HomeWork1.Task4;

// Реализовать метод, открывающий
// файл ./input.txt и сохраняющий в файл ./output.txt текст из input,
// где каждый латинский строчный символ заменен на
// соответствующий заглавный. Обязательно использование try с ресурсами.

public class Main {
    public static void main(String[] args) {

        MyEvenNumber myEvenNumber = new MyEvenNumber();


        try {
            //В метод setNumber передается целое число и если оно не четное то перехватывается Exception
            myEvenNumber.setNumber(14);
            System.out.println(myEvenNumber.getNumber());

            //Здесь нечетное число и поэтому ошибка
            myEvenNumber.setNumber(15);

        } catch (MyCheckEvenException e) {
            System.out.println(e.getMessage());
        }

        System.out.print("Проверка на отцовство myEvenNumber = " + myEvenNumber.CheckInstanceOf());



    }
}
