package HomeWork1.Task4;

public class MyEvenNumber {
    private int number;
    private MyPrimeNumber myPrimeNumber = null;

    protected MyEvenNumber() {
    }

    protected void setNumber(int number) throws MyCheckEvenException {
        //ПРОВЕРКА НА ЧЕТНОСТЬ, если число не четное выбрасывем ошибку
        if (number % 2 != 0) {
            throw new MyCheckEvenException();
        }
        //СОЗДАЕМ ИНСТАНС MyPrimeNumber.
        myPrimeNumber = new MyPrimeNumber();
        myPrimeNumber.setNumber(number);
    }

    protected String getNumber() {
        return myPrimeNumber.getNumber();
    }

    protected boolean CheckInstanceOf() {
        return myPrimeNumber.CheckInstanceOf();
    }

}