package HomeWork1.Task4;

//Инстанс для хранения четных чисел, наследник класса MyEvenNumber
public class MyPrimeNumber
        extends MyEvenNumber{

    private int number;

    protected MyPrimeNumber() {
    }

    @Override
    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String getNumber() {
        return "Создан инстанс MyEvenNumber со значением из MyEvenNumber = " + number;
    }

    //Проверка на instanceof
    public boolean CheckInstanceOf () {
       return this instanceof MyEvenNumber;
    }
}
