package HomeWork1.Task4;

public class MyCheckEvenException
        extends Exception{

    public MyCheckEvenException() {
        super("Ошибка! Число не четное");
    }

    public MyCheckEvenException(String message) {
        super(message);
    }

}
