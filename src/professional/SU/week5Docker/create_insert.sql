/*
Создать таблицу книг со следующими полями
- id ключ
- title- наименование книги
- author - автор книги
- date_added - дата добавления книги
 */

create table books
(
    id         serial primary key,
    title      varchar(30) NOT NULL,
    author     varchar(30) not null,
    date_added timestamp   not null
);
select *
from books;

insert into books(title, author, date_added)
values ('Недоросль5', 'Д. И. Фонвизин', now());

INSERT INTO books(title, author, date_added)
VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');

alter table books
alter column title type varchar(100);

alter table books
alter column author type varchar(100) using author::varchar(100);

INSERT INTO books
VALUES (3, 'Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
INSERT INTO books
VALUES (4, 'Сестра моя - жизнь', 'Б. Л. Пастернак', now());





