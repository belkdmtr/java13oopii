package professional.SU.week3ReflexAnnotations.annotations;

/*
Обращение к аннотации с помощью Reflection
Для проверки наличия аннотации у класса, существуют методы класса Class:
isAnnotationPresent()
Проверяет, есть ли у класса данная аннотация
getAnnotation()
Возвращает аннотацию, если она есть у класса, иначе null
getAnnotations()
Возвращает все аннотации, примененные к классу
getDeclaredAnnotations()
Как предыдущий, но не учитывает аннотации, унаследованные с помощью @Inherited

 */
public class AnnotationReflection {
    /*
    Задача 6.
    Создать класс, который по аннотации @ClassDescription из предыдущего задания выводит все описание на экран:
     */
    public static void main(String[] args) {
        printClassDescription(MyPerfectClass.class);
    }
    
    public static void printClassDescription(Class<?> cls) {
        if (!cls.isAnnotationPresent(ClassDescription.class)) {
            return;
        }
        ClassDescription classDescription = cls.getAnnotation(ClassDescription.class);
        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата создания: " + classDescription.date());
        System.out.println("Текущий номер версии: " + classDescription.currentRevision());
        System.out.println("Список проверяющих: ");
        for (String s : classDescription.reviewers()) {
            System.out.println(">>> " + s);
        }
    }
}
