package professional.SU.week3ReflexAnnotations.reflection.methods;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
Работа с методами
Методы для получения методов похожи на те, что были для полей:
getMethods(),
getDeclaredMethods(),
getMethod(),
getDeclaredMethod()
Смысл абсолютно тот же, только возвращаются методы

Основные методы класса Method:
getModifiers()
getName()
newInstance() - Создаёт новый инстанс класса, вызвав конструктор

Работа с конструкторами
Аналогично:
getConstructors
getDeclaredConstructors
getConstructor
getDeclaredConstructor

 */
public class ReflectionMethodsClass {
    public static void main(String[] args) {
        //Задача 4.
        //Сконструировать класс.
        //То есть вызвать конструктор класса и обработать все возможные ошибки.
        Class<Task4> cls = Task4.class;
        try {
            Constructor<Task4> constructor = cls.getDeclaredConstructor(int.class, String.class);
            Task4 result = constructor.newInstance(143, "Test Constructor Reflection");
            System.out.println(result);
            System.out.println(result.a);
            System.out.println(result.b);
        }
        catch (NoSuchMethodException e) {
            System.out.println("No such method: " + e.getMessage());
        }
        catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
