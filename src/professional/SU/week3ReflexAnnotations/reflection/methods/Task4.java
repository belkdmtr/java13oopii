package professional.SU.week3ReflexAnnotations.reflection.methods;

public class Task4 {
    int a;
    String b;
    
    public Task4(int a, String b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public String toString() {
        return "Task4{" +
               "a=" + a +
               ", b='" + b + '\'' +
               '}';
    }
}
