package professional.SU.week2GenericInterfaces.task1;

/*
Создать класс Pair, который умеет хранить два значения:
а) любого типа (T, U)
б) одинакового типа (T)
в) первое - только строка, второе - только число.
 */
public class Main {
    public static void main(String[] args) {
//        Pair<String, Integer> pair = new Pair<>();
//        pair.first = "Test first";
//        pair.second = 123;
//        pair.print();
//
//        Pair<Double, Character> pair2 = new Pair<>();
//        pair2.first = 2d;
//        pair2.second = 'C';
//        pair2.print();

//        Pair<String> pair = new Pair<>();
//        pair.first = "Test first";
//        pair.second = "Test second";
//        pair.print();
        
        Pair<String, Double> pair = new Pair<>();
        Pair<String, Integer> pair1 = new Pair<>();
        Pair<String, Float> pair2 = new Pair<>();
        Pair<String, Number> pair3 = new Pair<>();
//        Pair<String, String> pair4 = new Pair<>();
    
    
    }
}
