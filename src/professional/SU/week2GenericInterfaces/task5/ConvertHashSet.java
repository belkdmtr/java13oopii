package professional.SU.week2GenericInterfaces.task5;

import java.util.HashSet;
import java.util.TreeSet;

/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.

Про деревья:
 https://habr.com/ru/post/330644/
 
Структуры данных в картинках:
  https://habr.com/ru/post/128017/
  
  Книга:
  Структуры данных и алгоритмы - Роберт Лафоре
 */
public class ConvertHashSet {
    
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        TreeSet<T> result = new TreeSet<>();
        //Collections.addAll();
        //result.addAll(from);
        for (T elem : from) {
            result.add(elem);
        }
        return result;
    }
    
    private ConvertHashSet() {
    
    }
    
}
