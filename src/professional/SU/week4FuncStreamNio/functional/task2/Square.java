package professional.SU.week4FuncStreamNio.functional.task2;

@FunctionalInterface
public interface Square {
    int calculateSquare(int x);
}
