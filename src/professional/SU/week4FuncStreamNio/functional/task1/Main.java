package professional.SU.week4FuncStreamNio.functional.task1;


/*
Задача 1
Создать таймер, который считает время выполнения метода, используя Runnable.
 */
public class Main {
    public static void main(String[] args) {
        //обычный запуск
        Timer timer = new Timer();
        timer.measureTime(new SimpleSummator());
        System.out.println(timer.timeNanoSeconds);
        
        //анонимный класс (Java 7)
        Timer timeAnonymous = new Timer();
        timeAnonymous.measureTime(new Runnable() {
            @Override
            public void run() {
                long sum = 0;
                for (long i = 1; i <= 1_000_000_000; i++) {
                    sum += i;
                }
                System.out.println(sum);
            }
        });
        System.out.println(timeAnonymous.timeNanoSeconds);
        
        //java 8:
        //(parameter list) -> {lambda body}
        Timer lambdaTimer = new Timer();
        lambdaTimer.measureTime(() -> {
            long sum = 0;
            for (long i = 1; i <= 1_000_000_000; i++) {
                sum += i;
            }
            System.out.println(sum);
        });
        System.out.println(lambdaTimer.timeNanoSeconds);
        
    }
}
