package professional.SU.week4FuncStreamNio.functional.task5;

@FunctionalInterface
public interface MySuperPuperInterface<T> {
    T func(T value);
    
    default T func1(T value) {
        return value;
    }
}
