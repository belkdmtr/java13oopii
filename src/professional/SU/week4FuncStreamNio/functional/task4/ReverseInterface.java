package professional.SU.week4FuncStreamNio.functional.task4;

@FunctionalInterface
public interface ReverseInterface {
    String getReversedString(String initialString);
}
