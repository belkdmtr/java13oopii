package professional.SU.week4FuncStreamNio.functional.task3;

@FunctionalInterface
public interface MyInterface {
    double getPiValue();
}
