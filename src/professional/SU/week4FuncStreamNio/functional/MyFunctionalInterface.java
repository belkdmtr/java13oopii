package professional.SU.week4FuncStreamNio.functional;

@FunctionalInterface
public interface MyFunctionalInterface {
    double getValue();
    //Single Abstract Methods (SAM)
}
