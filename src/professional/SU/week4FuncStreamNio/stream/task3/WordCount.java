package professional.SU.week4FuncStreamNio.stream.task3;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
Вывести количество повторений каждой строки и саму строку
 */
public class WordCount {
    public static void main(String[] args) {
        List<String> names = List.of("Sam", "James", "Elena", "James", "Joe", "Sam", "James");
        
        //1 способ
        Set<String> unique = new HashSet<>(names);
        for (String key : unique) {
            System.out.println(key + ": " + Collections.frequency(names, key));
        }
        
        //2 способ
        Map<String, Long> frequencyMap = names.stream()
              .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(frequencyMap);
        //TODO: за каникулы выучить SQL!!!!!!!
              /*
              select name, count(*)
              from names
              group by name;
              
              1 row: Andrei 1
              2 row: Andrei 1
              3 row: Dmitry 1
              ->
              1 row: Andrei, 2
              2 row: Dmitry, 1
               */
        
        //3 способ
        //TODO: parallelStream
        Map<String, Integer> counts = names.parallelStream()
              .collect(Collectors.toConcurrentMap(k -> k, v -> 1, Integer::sum));
        System.out.println(counts);
    }
}
