package professional.SU.week4FuncStreamNio.DefaultFuncinterfaces;

/*
TODO: Default Functional Interfaces (...Дмитрий, например что-то типа такого...)
 */

@FunctionalInterface
public interface AndreyDefaultInterfaces<T> {
    T func(T value);

    default T func1(T value) {
        return value;
    }
}


