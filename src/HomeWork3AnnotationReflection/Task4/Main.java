package HomeWork3AnnotationReflection.Task4;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

/*
       СХЕМА НАСЛЕДОВАНИЯ - все стикается в classTwo
*********************************
classOne <--- interfaceForOne   *
    v                           *
classTwo <--- interfaceForTwo   *
                    ^           *
              interfacesThree   *
                    ^           *
              interfacesFour    *
*********************************

Результат работы программы:
interfaceForTwo
interfacesThree
interfaceForOne
interfacesFour
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //Вызвать метод для класса classTwo, что бы получить все интрефейсы
        List<Class<?>> result = getAllInterfaces(classTwo.class);

        //Печать всего списка result
        for (Class<?> cls : result) {
            System.out.println(cls.getSimpleName());
        }
    }

    //Метод получающий все интерфейсы от класса и все интерфейсы от интерфейсов
    public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
        List<Class<?>> interfaces = new ArrayList<>();
        int count = 0; //счетчик нужен, что бы достучаться до полученного интерфейса из класса в массиве
        while (clazz != Object.class) { //итерируемся, пока не получим Object класс
            //Добавим список всех интерфейсов унаследованных от класса
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            //Передадим в метод getSubInterfaces() полученный интерфейс у класса,
            //что бы найти в нем еще подинтрефейсы и добавим их в список
            interfaces.addAll(getSubInterfaces(interfaces.get(count)));
            //Перейдем к следующему классу пока не доберемся до Object
            clazz = clazz.getSuperclass();
            count++; //счетчик ++
        }
        return interfaces;
    }

    //Метод для поиска субинтерфейсов (интерфейсов унаследованных от интерфейсов)
    public static List<Class<?>> getSubInterfaces(Class<?> inInterfaces) {
        List<Class<?>> subInterfaces = new ArrayList<>();
        subInterfaces.addAll(Arrays.asList(inInterfaces.getInterfaces()));
        return subInterfaces;
    }
}
