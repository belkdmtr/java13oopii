package HomeWork3AnnotationReflection.Task1and2;

public class Main {
    public static void main(String[] args) {

        printMyCalsAnnotation(MyAnnotationClass.class);

    }


    /*
    Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
    любом переданном классе и выведет значение, хранящееся в аннотации, на
    экран.
     */
    public static void printMyCalsAnnotation(Class<?> cls) {
        //Проверка, что бы у класса MyAnnotationClass содержалась АННТОТАЦИЯ
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        //Получаем доступ к аннтоации через рефлексию.
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println(isLike.value());
    }

}
