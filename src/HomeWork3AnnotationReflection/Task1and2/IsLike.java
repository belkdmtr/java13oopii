package HomeWork3AnnotationReflection.Task1and2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
Создать аннотацию @IsLike, применимую к классу во время выполнения
программы. Аннотация может хранить boolean значение.
 */

@Retention(RetentionPolicy.RUNTIME) // что бы можно было обратиться к аннотации через рефлексию
public @interface IsLike {
      boolean value() default false; //Аннтоация содержит булевую переменную
}
