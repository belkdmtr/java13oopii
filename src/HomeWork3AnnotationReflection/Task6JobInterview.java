package HomeWork3AnnotationReflection;
/*
Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Условия для правильной скобочной последовательности те, же что и в задаче 1,
но дополнительно:

● Открывающие скобки должны быть закрыты однотипными
закрывающими скобками.
● Каждой закрывающей скобке соответствует открывающая скобка того же
типа.

 */

import java.util.*;

public class Task6JobInterview {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        System.out.println(checkStr(str));
    }

    public static boolean checkStr(String str) {
        //Создадим мапу для набора всех скобок
        Map<Character, Character> mapStr = new HashMap<>();
                 //KEY   VALUE
        mapStr.put(')', '(');
        mapStr.put('}', '{');
        mapStr.put(']', '[');

        //Создадим стек Deque и в качестве реализации использую LinkedList
        Deque<Character> charactersStack = new LinkedList<>();

        for (char value : str.toCharArray()) {
            //Если скробка открывающаяся то добавим ее в стек
            if (mapStr.containsValue(value)) {
                charactersStack.push(value);
            } else {
                //Если мапа содержит одно из 3х значений, то...
                if (mapStr.containsKey(value)) {
                    //Принципиальная часть, если стек пустой ИЛИ первая скобка в стеке сравнивается
                    //и не похожа на своего двойника, то последовательность не верная
                    System.out.println(charactersStack.peek() + " " + mapStr.get(value));
                   if (charactersStack.isEmpty() || charactersStack.peek() != mapStr.get(value)) {
                       return false;
                   }
                   //Удалим из стека парную скобку
                   charactersStack.pop();
                }
            }
        }
        //если все условия успешны, стек пустой возращаем true так как парные скобки были удалены
        return charactersStack.isEmpty();
    }
}
/*
{()[]()}
{)(}
{([)]}
[}
{}
[{(){}}][()]{}
 */