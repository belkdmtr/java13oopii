package HomeWork3AnnotationReflection.Task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args)  {

       Class<? extends APrinter> cls = APrinter.class;


        try {
            Method method = cls.getDeclaredMethod("print", int.class);
            method.invoke(cls.newInstance(), 50);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Метод c заданными параметрами не найден");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Не получилось вызвать метод");
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Ошибка");
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Введено не верное значение");
        } catch (InstantiationException e) {
            throw new RuntimeException("Не удалсь создать инстанс");
        }

    }


}
