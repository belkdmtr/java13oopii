package HomeWork3AnnotationReflection;

/*
Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.

Строка является правильной скобочной последовательностью, если:
● Пустая строка является правильной скобочной последовательностью.
● Пусть S — правильная скобочная последовательность, тогда (S) есть
правильная скобочная последовательность.
● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
есть правильная скобочная последовательность.
 */

/*
Входные данные
(()()())        -       true
)(              -       false
(()             -       false
((()))          -       true
 */

import java.util.*;

public class Task5JobInterview {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        System.out.println(checkStr(str));

        //значения скобок ( = 40;
        //значения скобок ) = 41;
    }

    public static boolean checkStr(String str) {
        //Создадим стек Deque и в качестве реализации использую LinkedList
        Deque<Character> charactersStack = new LinkedList<>();

        for (char value : str.toCharArray()) {
            //Если скробка открывается до добавим ее в стек
            if (value == 40) {
                charactersStack.push(value);
            } else {
                //Иначе? если у нас закрывающая скобка
                if (value == 41) {
                    //Если стек пустой, а закрывающаяся скобка еще есть (т.е. не четное число скобок),
                    //а так же если мы ничего не добавили в стек или удалять уже нечего, тогда false
                    if (charactersStack.isEmpty()) {
                       return false;
                    }
                    //иначе удаляем парную скобку
                    charactersStack.pop();
                }
            }
        }
        //если все условия успешны, стек пустой возращаем true так как парные скобки были удалены
        return charactersStack.isEmpty();
    }
}