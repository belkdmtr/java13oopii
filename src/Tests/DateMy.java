package Tests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateMy {
    public static void main(String[] args) throws ParseException {

        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date date = format.parse("2022-12-10");
        System.out.println(date);

    }
}
