package HomeWork2.Task2;

/*
НЕ ПРОВЕРЯТЬ НЕ ПРОВЕРЯТЬ (НЕ ПОНЯЛ ЗАДАНИЯ)
СДЕЛАЛ НЕ ТО!
ПРОГРАММА ПРОВЕРЯТ ЗЕРКАЛЬНО ЛИ СЛОВО ИЛИ НЕТ!
ПРАВИЛЬНАЯ ПРОГРАММА В КЛАССЕ MAIN !!!!!!!!!!!!
 */

import java.util.*;

public class BadMainDoNotCheck {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s;
        String t;

        //s = scanner.toString();
        //t = scanner.toString();

        s = "Dmitrii";
        t = "iirtimD";

        //Перевод строки в массив строк String при помощи регулярки.
        String[] sArray = s.split("");
        String[] tArray = t.split("");

        //Создаем два интрефейса, реализуем их при помощи ArrayList и записываем наши буквы в List.
        List<String> stringList1 = new ArrayList<>(Arrays.asList(sArray));
        List<String> stringList2 = new ArrayList<>(Arrays.asList(tArray));

        //Промежуточный вывод массива символов двух строк
        System.out.println("Первая строка: " + stringList1.toString());
        System.out.println("Вторая строка: " + stringList2.toString());

        //Вызов метода для сравнения двух строк
        System.out.println( comparingTwoStrings(stringList1, stringList2));
    }

    //Метод проверки на анаграммность строк
    public static <T> boolean comparingTwoStrings(List<T> stringList1, List<T> stringList2) {

        if (stringList1.size() != stringList2.size()) { //false - если строки не равны.
            return false;
        } else {
            if (stringList1.equals(stringList2)) { //false - если строки равны
                return false;
            }
            else {
                Collections.reverse(stringList2);
                if (stringList1.equals(stringList2)) {
                    return true; //true - и только так если слова зеркальные
                }
            }
            return false;
        }
    }
}
