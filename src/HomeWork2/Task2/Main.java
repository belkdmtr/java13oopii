package HomeWork2.Task2;

/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s;
        String t;

        //Набор слов с консоли
        //s = scanner.toString();
        //t = scanner.toString();

        //Входные данные
        s = "Listen";
        t = "Silent";

        System.out.println("Введены две сроки: \n" + s + "\n" + t);

        //Переведем строки в массив char и сделаем символы маленькие
        char[] sArray = s.toLowerCase().toCharArray();
        char[] tArray =t.toLowerCase().toCharArray();

        //Найдем сууму всех букв в 2х циклах
        int sSum = 0;
        int tSum = 0;

        //Считаем сумму всех знаков
        for (char value : sArray) {
            sSum += value;
        }

        for (char value : tArray) {
            tSum += value;
        }

        //Логика поиска Анаграммы, если длина строк не равна или сумма chart символов не равна
        //то очевидно, что слова состоят из разных букв и не являются Анаграммой
        System.out.print("Введенные слова являются анаграммой: ");
        if (sArray.length != tArray.length && sSum != tSum) {
            System.out.println(false);
        } else {
            System.out.println(true);
        }
    }
}
