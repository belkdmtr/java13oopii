package HomeWork2.Task4;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document() {
    }

    public void setNewDocument(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    //Геттер для полкчения ID книги
    public int getId() {
        return id;
    }


    @Override
    public String toString() {
        return " Название книги: " +
                name +
                ", всего в ней страниц - " + pageCount;
    }
}
