package HomeWork2.Task4;

/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Создаим коллекцию List, которую реализует ArrayList
        List<Document> documents = new ArrayList<>();

        //Добавим три книги в нашу коллекцию List
        documents.add(new Document());
        documents.add(new Document());
        documents.add(new Document());

        //Опишем книги в коллекции ID НАЗВАНИЕ и Кл-во СТРАНИЦ
        documents.get(0).setNewDocument(1, "Book_1", 100);
        documents.get(1).setNewDocument(2, "Book_2", 200);
        documents.get(2).setNewDocument(3, "Book_3", 300);

        //Произведем красивый вывод результата программы
        //в начале передадим все ключи мапы в коллекцию Set keys,
        //что бы потом по ключу вывести рекзультат
        Set<Integer> keys = organizeDocuments(documents).keySet();

        //А теперь по ключу выведим наши обьекты = книги
        for (Integer key: keys) {
            System.out.println("Ключ=" + key + " " + organizeDocuments(documents).get(key));
        }

    }

    //Метод реализующий перевод из из ArrayList в HashMap, где на вход пришел List,
    //а возращаем Мапу
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map <Integer, Document> documentMap = new HashMap<>();
        //ID для KEY Мапы мы берем из объекта документов getId
        for (int i = 0; i < documents.size(); i++) {
            documentMap.put(documents.get(i).getId(), documents.get(i));
        }
        return documentMap;
    }

}
