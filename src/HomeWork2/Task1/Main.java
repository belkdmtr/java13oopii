package HomeWork2.Task1;

/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //Создали массив строк
        ArrayList<String> arrayList = new ArrayList<>();

        //Заполнили массив строк
        arrayList.add("Elephant");
        arrayList.add("Dog");
        arrayList.add("Cat");
        arrayList.add("Eagle");
        arrayList.add("Eagle");
        arrayList.add("Elephant");

        //Предварительно выведем массив строк
        System.out.println("Начальные занчения ArrayList: " + arrayList.toString());

        //Вызов метода принимающего ArrayList<T>, а возвращает
        //набор уникальных элементов этого массива Set<T> setList
        System.out.println("Набор уникальных элементов ArrayList: " + arrayСonverter(arrayList));


    }

    //Метот Generic <T> возращает Set<T>  принимает на вход ArrayList<T>
    public static <T> Set<T> arrayСonverter(ArrayList<T> arrayList) {
        // Сразу создаем Set реализуемый TreeSet с уже переданными значениями массива arrayList
        Set<T> setList = new TreeSet<>(arrayList);
        //Возращаем Set в виде коллекции SET
        //return setList.toString();
        return setList;
    }
}
