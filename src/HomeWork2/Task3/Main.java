package HomeWork2.Task3;

/*
Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        PowerfulSet powerfulSet = new PowerfulSet();

        //Заполнение Сета 1
        Set<Integer> set1 = new HashSet<>(addSet1());
        System.out.println("Первый сет " + set1);
        //Заполнение Сета 2
        Set<Integer> set2 = new HashSet<>(addSet2());
        System.out.println("Второй сет " + set2);

        //a) Результат работы первой задачи
        //Вернуть {1, 2}
        System.out.print("Возвращает пересечение двух наборов " + powerfulSet.intersection(set1, set2) + "\n");

        //Обновим сеты
        set1.addAll(addSet1());
        set2.addAll(addSet2());

        //б) Результат работы второй задачи
        //Вернуть {0, 1, 2, 3, 4}
        System.out.print("Возвращает объединение двух наборов " + powerfulSet.union(set1, set2) + "\n");


        //Обновим сеты
        set1.addAll(addSet1());
        set2.addAll(addSet2());

        //с) Результат работы третий задачи
        //Вернуть {3}
        System.out.print("Возвращает элементы первого набора без тех," +
                "что во втором " + powerfulSet.relativeComplement(set1, set2) + "\n");

    }


    //Методы заполнения сетов
    public static <T> Set<Integer> addSet1() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        return set;
    }

    public static <T> Set<Integer> addSet2() {
        Set<Integer> set = new HashSet<>();
        set.add(0);
        set.add(1);
        set.add(2);
        set.add(4);
        return set;
    }
}
