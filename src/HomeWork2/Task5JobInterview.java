package HomeWork2;

/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов..
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.

Входные данные
words = ["the","day","is","sunny","the","the","the","sunny","is","is","day"]
k = 4

Выходные данные
["the","is","sunny","day"]
 */

import java.util.*;

public class Task5JobInterview {
    public static void main(String[] args) {

        //Задан массив строк и значение К для количества выводимых значений
        String[] arr = {"the","day","is","sunny","the","the","the","sunny","is","is","day"};
        //String[] arr = {"the","day","the","day","the","day","sunny","sunny","sunny"};
        int k = 4;

        //Вызываем основной метод
        frequent(arr, k);
    }

    /*
    Создаем SortedMap и заполняем ее из массива в цикле
     */
    //public static List<Map.Entry<String, Integer>> frequent(String[] arr, int k) {
    public static void frequent(String[] arr, int k) {
        //В SortedMap мы уже отсортировали наши слова в лексеграфическом порядке
        SortedMap<String, Integer> map = new TreeMap<>();
        for (String value : arr) {
            int counter = 1; //счетчик для запасии значений, сколько раз повторяется наш ключ
            if (map.containsKey(value)) { //Если мапа содержит уже наш ключ /в данном случае слово/
                counter = map.get(value); //То мы получим значение по ключу value, а там у нас цифры
                counter ++; //b прибавим +1 к уже существующему значению, тем самым покажем, сколько раз
                //использовался этот ключ
            }
            map.put(value, counter); //перезапишем значение напротив ключа
        }

        /*Теперь, что бы вывести слова по убыванию от большего меньшего, сохраним нашу мапу в интерфейс лист
        и там же переопределим компоратор
         */
        List <Map.Entry<String, Integer>> mapList = new ArrayList<>(map.entrySet()); //закинули мапу в лист
        Collections.sort(mapList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue()); //сортировка от большего к меньшему по значению
            }
        });

        //Если к > длины массива, то выводим все элементы
        if (k > mapList.size()) {
            k = mapList.size();
        }

        //Печатаем элементы в консоль
        for (int i = 0; i < k; i++) {
            System.out.print(mapList.get(i) + " ");
        }
    }
}
