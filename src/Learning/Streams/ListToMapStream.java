package Learning.Streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListToMapStream {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.add(3); //ДУБЛИКАТ КЛЮЧА 3

/*
        list.stream()
                .filter(digit -> digit >= 2 && digit <=4)
                .map(digit -> digit * 2 + " ")
                .forEach(System.out::print);

 */

        //СТРИМ НЕ МЕНЯЕТ КОЛЛЕКЦИЮ ОТ КОТОРОЙ ОН ВЫЗВАЛСЯ
        //СТРИМ ВОЗРАЩАЕТ КОПИЮ!!!!!!
        System.out.println();

        //2. ИЗМЕНЕННАЯ КОЛЛЕКЦИЯ
        List<Integer> anotherList = list.stream()
                .filter(digit -> digit >= 2 && digit <=4)
                .map(digit -> digit * 2)
                .collect(Collectors.toList());

        //3. Мапа
        System.out.println(list);
        Map<Integer, Integer> newMap = list.stream()
                //Преобразование в Мапу с промежуточным выводов старого и новго значений
                            //коюч = эл. массива    //зачение получаем из (v) и если у нас дубликат ключа то (o1 + o2)
                .collect(Collectors.toMap(k -> k, v -> v, (o1, o2) -> {
                    System.out.println("o1=" + o1);
                    System.out.println("o2=" + o2);
                    return o1 + o2;
                }));
        System.out.println(newMap);

        //System.out.println(list);
        //System.out.println(anotherList);
        //System.out.println(newMap);




    }
}
