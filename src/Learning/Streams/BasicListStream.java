package Learning.Streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BasicListStream {
    public static void main(String[] args) {

        //Стрим с условием и форматированным выводом ПРОСТО СЕКС!
        List<String> list = Stream.of(1, 2, 3, 4, 5)
                .filter(digit -> digit > 2)
                .map(digit -> "newStr " + digit)
                .collect(Collectors.toList());


        list.forEach(System.out::println);

        /*

        //1. Преобразование из INT в STRING ОБЫЧНЫЙ МЕТОД

        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5)
        Stream<String> newStream = stream.map((Integer digit) -> {
           return "str" + digit;
        });

        newStream.forEach((String str) -> {
            System.out.println(str);
        });

        //2. ФИЛЬТРАЦИЯ ЭЛЕМЕНТОВ
        stream
                .filter((Integer digit) -> { //Предикат фильтрует значения <2
            if (digit > 2) {
                return true;
            } else {
                return false;
                }
            })
                .forEach(System.out::println);

         */

    }
}
