package Learning.Streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapGroupByStream {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            list.add(i);
        }


        //partitioningBy - РАЗБИВАЕТ НА ДВЕК КАТЕГОРИИ
        //Какой же это СЕКС!
        //Разбиваем мапу на два списка, где под ключем фальш и тру будут значения согласно условию (digit <= 2)
        Map<Integer, List<Integer>> newMap = list.stream()
                .collect(Collectors.groupingBy((Integer digit) -> {
                    return digit % 3;
                }));

        System.out.println(newMap);

    }
}
