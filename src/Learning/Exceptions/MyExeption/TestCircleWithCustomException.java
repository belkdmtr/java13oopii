package Learning.Exceptions.MyExeption;

/**
 * Чтобы создать исключение InvalidRadiusException, необходимо передать радиус.
 * Следовательно, метод setRadius() класса CircleWithException можно изменить,
 * как показано в программе TestCircleWithCustomException.
 */

public class TestCircleWithCustomException {
    public static void main(String[] args) {
        try {
            new CircleWithCustomException(5);
            new CircleWithCustomException(-5);
            new CircleWithCustomException(0);
        }
        catch (InvalidRadiusException ex) {
            System.out.println(ex);
        }

        System.out.println("Количество созданных объектов: " +
                CircleWithCustomException.getNumberOfObjects());
    }
}
