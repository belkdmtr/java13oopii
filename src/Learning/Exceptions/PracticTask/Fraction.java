package Learning.Exceptions.PracticTask;

/**
 * Создайте класс Fraction с двумя целочисленными final-полями: числителем и знаменателем.
 * Конструктор этого класса создает дроби при заданных значениях, но выбрасывает исключение
 * NullDenominatorException при нулевом знаменателе. Для этого дополнительно создайте класс
 * проверяемого исключения NullDenominatorException, объекты которого выбрасываются,
 * если знаменатель объекта типа Fraction равен 0, а также напишите тестовый класс,
 * который проверяет эти классы.
 */

public class Fraction {
    /**
     * В классе Fraction определим два целочисленных final-поля: числитель и знаменатель.
     */
    private final int numerator; // числитель
    private final int denominator; // знаменатель

    /**
     * Добавим в класс Fraction конструктор с двумя параметрами.
     * В конструкторе класса Fraction добавим проверку знаменателя дроби на ноль.
     * Объявим исключение, добавив его в сигнатуру метода - throws NullDenominatorException
     */
    public Fraction(int numerator, int denominator) throws NullDenominatorException {
        this.numerator = numerator;
        this.denominator = denominator;

        if (denominator == 0) {
            throw new NullDenominatorException("Знаменатель объекта типа Fraction равен 0");
        }
    }

    public void getResult() {
        System.out.println(numerator/denominator);
    }


    /**
     * Переопределим метод toString() в классе Fraction для отображения в виде строки экземпляра класса.
     * @return //отображает в виде строки экземпляра класса
     */

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        return numerator + "/" + denominator;
    }





}
