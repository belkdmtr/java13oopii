package Learning.Exceptions.PracticTask;

public class TestFraction {
    public static void main(String[] args) {
        try {
            Fraction fraction = new Fraction(9, 8);

            System.out.print(fraction.toString() + " равно ");
            fraction.getResult();

        } catch (NullDenominatorException e) {
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }

    }
}
