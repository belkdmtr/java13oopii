package Learning.Exceptions.PracticTask;

/**
 * Создадим новый класс исключений NullDenominatorException,
 * который унаследуем от класса Exception.
 */

public class NullDenominatorException extends Exception{

    /** Создает исключение с сообщением message */
    public NullDenominatorException(String message) {
        //В конструкторе с параметром вызовем конструктор
        // родительского класса с помощью ключевого слова super.
        super(message);

    }


}
