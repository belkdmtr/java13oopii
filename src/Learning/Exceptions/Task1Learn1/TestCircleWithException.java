package Learning.Exceptions.Task1Learn1;

/**
 * В этом примере решения задачи показано объявление,
 * выбрасывание и перехват исключений с помощью изменения метода setRadius()
 * класса Circle из программы TestCircleWithPrivateDataFields, которая была представлена
 * в разделе «Объекты и классы». Новый метод setRadius() будет выбрасывать
 * исключение, если радиус отрицательный.
 *
 * В программе TestCircleWithException определяется новый класс круга с именем
 * CircleWithException, который совпадает с Circle из программы TestCircleWithPrivateDataFields,
 * за исключением того, что метод setRadius(double newRadius) выбрасывает исключение
 * IllegalArgumentException, если аргумент newRadius отрицателен.
 */

public class TestCircleWithException {
    /** Метод main */
        public static void main(String[] args) {
            try {
                CircleWithException c1 = new CircleWithException(5);
                CircleWithException c2 = new CircleWithException(-5);
                CircleWithException c3 = new CircleWithException(4);
            }
            catch (IllegalArgumentException ex) {
                System.out.println(ex);
            }

            System.out.println("Количество созданных объектов: " + CircleWithException.getNumberOfObjects());
        }
    }