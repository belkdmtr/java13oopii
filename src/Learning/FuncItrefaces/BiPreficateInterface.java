package Learning.FuncItrefaces;

import java.util.function.BiPredicate;

public class BiPreficateInterface {
    public static void main(String[] args) {

        BiPredicate<Integer, Integer> biTest = (Integer leftFigit, Integer rightDigit) -> {
          if (leftFigit + rightDigit >= 7) {
              return true;
          } else {
              return false;
          }
        };

        System.out.println(biTest.test(2, 3));
        System.out.println(biTest.test(5, 6));

    }
}
