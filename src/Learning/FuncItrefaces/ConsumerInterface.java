package Learning.FuncItrefaces;

import java.util.function.Consumer;

public class ConsumerInterface {
    public static void main(String[] args) {

        //Коньсюмер принимает значение и ничего не возращает
        Consumer<String> test = (String str) -> {
            System.out.println(str);
        };

        test.accept("Переданная строка в функциональный интрефейс Consumer");

    }
}
