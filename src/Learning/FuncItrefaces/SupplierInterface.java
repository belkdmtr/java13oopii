package Learning.FuncItrefaces;

import java.util.function.Supplier;

public class SupplierInterface {
    public static void main(String[] args) {

        //Не принимает никаких значений
        Supplier<Integer> teset = () -> {
            return 12;
        };

        System.out.println(teset.get());

        Supplier<Exception> teset2 = () -> {
            return new Exception("Хьюстон у нас проблемы");
        };

        Exception e = teset2.get(); //саплаер передал ошибку
        throw new RuntimeException(e); //перебросили ошибку



    }
}
