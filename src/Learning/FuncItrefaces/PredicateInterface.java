package Learning.FuncItrefaces;

import java.util.function.Predicate;

public class PredicateInterface {
    public static void main(String[] args) {

        //Обычный сопосб написания предиката
        Predicate<Integer> test1 = new Predicate<Integer>() { //Созданине предиката реализуемого анонимным классом
            @Override
            public boolean test(Integer digit) { //поменяли integer на digit
                if (digit >= 7) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        //Написание предиката через лямбды
        Predicate<Integer> test2 = (Integer digit) -> {
            if (digit >= 7) {
                return true;
            } else {
                return false;
            }
        };

        System.out.println(test1.test(10));
        System.out.println(test2.test(5));





    }
}
