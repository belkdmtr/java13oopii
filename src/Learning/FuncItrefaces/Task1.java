package Learning.FuncItrefaces;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;

public class Task1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("333");
        list.add("21");
        list.add("4");

        UnaryOperator<String> lambda = (String str) -> {
          return  str + "_" + str;
        };

        list.replaceAll(lambda);

        //Метод forEach вывести весь массив
        list.forEach((String str) -> {
            System.out.println("item = " + str);
        });

        //КОМПОРАТОР
        /*
        Сравнивает в лексикографичесокм порядке (если O1 < O2 возращаем = -1
                                                 если O1 < O2 возращаем = 0<
                                                 если O1 = O2 возращаем = 0
                                                 */

         //Компоратор обычнм способом:
         Comparator<String> comparator = (String o1, String o2) -> {
             //сортировка по длине слова
             int o1Size = o1.length();
             int o2Size = o2.length();
             if (o1Size == o2Size) {
                return 0;
             }
             //По возростанию сортировка
             if (o1Size < o2Size) {
                 return -1;
             } else {
                 return 1;
             }
        };

        //Вызов комопртаора
        list.sort(comparator);
        list.forEach((String str) -> {
            System.out.print(str);
            System.out.println(" ");
        });

        /*
        //Компортаор через лябду
        Comparator<String> comparator = String::compareTo;

        //Печать форич через лябду
        list.forEach(System.out::println);

         */



    }
}
