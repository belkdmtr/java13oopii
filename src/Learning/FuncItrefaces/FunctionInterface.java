package Learning.FuncItrefaces;

import java.util.function.Function;

public class FunctionInterface {
    public static void main(String[] args) {

        //Принимает на входи Integer и приобразует в String
        Function<Integer, String> test = (Integer digit) -> {
          return String.valueOf(digit);
        };

        System.out.println("Результат " + test.apply(20));

    }
}
