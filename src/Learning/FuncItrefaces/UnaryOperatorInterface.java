package Learning.FuncItrefaces;

import java.util.function.UnaryOperator;

public class UnaryOperatorInterface {
    public static void main(String[] args) {

        //UnaryOperator наследуется от Function
        UnaryOperator<Integer> test = (Integer digit) -> {
          return digit * 2;
        };

        System.out.println("Результат " + test.apply(20));


    }
}
