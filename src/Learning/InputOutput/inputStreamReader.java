package Learning.InputOutput;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class inputStreamReader {
    public static void main(String[] args) throws IOException {

        InputStream inputStream = System.in;
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        int symbol = inputStreamReader.read();
        System.out.println("Symbol = " + symbol + " " + (char) symbol);
    }
}

/**
 * Ф
 * Symbol = 1060 Ф
 */
