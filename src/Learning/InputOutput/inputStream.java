package Learning.InputOutput;

import java.io.IOException;
import java.io.InputStream;

public class inputStream {
    public static void main(String[] args) throws IOException {

        InputStream inputStream = System.in;

        int symbol;
        // ЗАПОМНИТЬ КОНСТРУКЦИЮ - ПОЗВОЛЯЕТ СЧИТАТЬ СИМВОЛ БОЛЬШЕ 1-го БАЙТА (так как read() читает 1 байт максимум)
        // Цикл будет выпоняться до тех пор, пока не будет достигнут конец потока
        while ((symbol = inputStream.read()) != -1) {
            System.out.println("Символ = " + symbol + " (" + (char) symbol + ")");
        }

    }
}

/**
 * Ф
 * Символ = 208 (Ð)
 * Символ = 164 (¤)
 * Символ = 10 (     --- ЭТО клавиша Enter которую мы нажимаем,
 * )                 --- когда вводим число
 */
