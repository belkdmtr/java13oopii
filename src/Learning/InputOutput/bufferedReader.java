package Learning.InputOutput;

import java.io.*;

public class bufferedReader {
    public static void main(String[] args) throws IOException {

        // Оборачиваем наш поток FileReader в буферезированный вывод
        BufferedReader reader = new BufferedReader(new FileReader("output.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("input.txt"));

        // Считал первую строку
        String line = reader.readLine();

        // Если строка не пустая (не вернула -1) я начинаю в цикле выводить строки
        while (line != null) {
            writer.write(line.toUpperCase());
            line = reader.readLine();
            if (line != null) {
                writer.write("\n");
            }
        }
        reader.close();
        writer.close();
    }
}
