package Learning.InputOutput;

import java.io.FileReader;
import java.io.IOException;

public class fullFileRead {
    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader("output.txt");

        int symbol = reader.read(); //Считывает 1 й символ!

        // Далее считываются последующие символы
        while (symbol != -1) {
            System.out.print((char) symbol);
            // после каждого чтения нам нужно считать еще один символ
            symbol = reader.read();
        }
        reader.close();
    }
}
