package Learning.InputOutput;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class fileRead {
    public static void main(String[] args) throws IOException {

        File file = new File("output.txt");

        System.out.println("Файл существует? " + file.exists());
        System.out.println("Это файл? " + file.isFile());
        System.out.println("Это директория? " + file.isDirectory());


        FileWriter writer = new FileWriter("output.txt"); // ОТКРЫЛИ НЕ СУЩЕСТВУЮЩИЙ ФАЙЛ
        writer.write("Филигранно просто!"); // ЗАПИСАЛИ В НЕГО
        writer.close(); // ЗАКРЫЛИ ЗАПИСЬ

        // СЧИТЫВАЕТ ОДИН СИМВОЛ
        FileReader reader = new FileReader("output.txt");
        int symbol = reader.read(); //Считывает символ!
        reader.close();
        System.out.println("Считаный символ " + symbol + " (" + (char) symbol + ")");


    }
}
