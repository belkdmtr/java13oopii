package Learning.Comporators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<User> userList = new ArrayList<>();
        userList.add(new User("Wing", 44, "St. Petersburg"));
        userList.add(new User("Dima", 50, "Rosotv-on-Don"));
        userList.add(new User("Zebra", 22, "Moscow"));
        userList.add(new User("Lida", 15, "Moscow"));

        System.out.println(userList);

        Comparator<User> comparatorByAge = new UserByAgeComporator();
        userList.sort(comparatorByAge);

        System.out.println(userList);

        Comparator<User> comparatorByCity = new UserByCityComporator();
        userList.sort(comparatorByCity);

        System.out.println(userList);





    }
}
