package Learning.Comporators;

import java.util.Comparator;

public class UserByCityComporator implements Comparator<User> {

    //Реализация метода compareTo
    @Override
    public int compare(User o1, User o2) {
       return o1.getCity().compareToIgnoreCase(o2.getCity());
    }
}
