/*
 Придумать как хранить в postgres следующую архитектуру:
Ирина, подруга Пети, решила создать свой бизнес по продаже цветов. Начать
она решила с самых основ: создать соответствующую базу данных для своего
бизнеса. Она точно знает, что будет продавать Розы по 100 золотых монет за
единицу, Лилии по 50 и Ромашки по 25.
Помимо этого, ей хочется хранить данные своих покупателей (естественно они
дали согласие на хранение персональной информации). Сохранять нужно Имя
и Номер телефона.
И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не
продумала поля, но она точно хочет следовать следующим правилам:
● в рамках одного заказа будет продавать только один вид цветов
(например, только розы)
● в рамках одного заказа можно купить от 1 до 1000 единиц цветов.
А также она представляет, какие запросы хочет делать к базе:
1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
3. Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
Выполненное домашнее задание должно содержать:
1. Sql-скрипты создания таблиц с необходимыми связями
2. Sql-скрипты наполнения таблиц (тестово добавить несколько строк данных для
последующей проверки select’ов)
3. Sql-скрипты получения данных в соответствии с условием
 */

-- 1. Sql-скрипты создания таблиц с необходимыми связями
-- 2. Sql-скрипты наполнения таблиц (тестово добавить несколько строк данных для последующей проверки select’ов)
-- Создадим таблицу с покупетелями

create table customer
(
customer_id     bigserial primary key,  -- id пользователя в системе
customer_name   varchar(100) not null,  -- имя покупателя не может быть пустым
customer_phone  varchar(11)             -- номер телефона покупателя (может быть не заполнен)
);

-- наполним таблицу customer
insert into customers(customer_name, customer_phone)
values ('Иван', '89034004041');

insert into customers(customer_name, customer_phone)
values ('Мария', '89058008082');

insert into customers(customer_name, customer_phone)
values ('Юлий', '89068006063');

insert into customers(customer_name, customer_phone)
values ('Павел', '89077007074');

insert into customers(customer_name, customer_phone)
values ('Алла', '89077007074');
-------------------------------------------------------------------

-- Создадим таблицу с товарами
create table product
(
    product_id      bigserial primary key,     -- id товара в системе
    product_title   varchar(25) not null,      -- наименование товара
    product_rate    int not null               -- стоимость товара
);

-- наполним таблицу product
insert into products(product_title, product_rate)
values ('Розы', 100);

insert into products(product_title, product_rate)
values ('Лилии', 50);

insert into products(product_title, product_rate)
values ('Ромашки', 25);

-------------------------------------------------------------------

-- Создадим таблицу заказов orders, где в рамках одного заказа будет продавать только один вид цветов
-- для этого реализуем таблицу orders со связью (один-ко-многим) по отношению к таблице products и к таблице customers,
-- в рамках одного заказа можно купить от 1 до 1000 единиц цветов
create table if not exists orders -- если такой таблицы orders не существует, то создать таблицу orders
(
    order_id        bigserial primary key,                          --id заказа в системе
    customer_id     integer references customers (customer_id),     --(foreign key) вторичный ключ ссылается на первичный ключ (customer_id) таблицы (customers)
    product_id      integer references products (product_id),       --(foreign key) вторичный ключ ссылается на первичный ключ (product_id) таблицы (products)
    --далее мы ограничим максимльное количество заказа от 1 до 1000 и присвоим ограничению (check) отдельное имя (allowed_volume)
    volume_of_order numeric constraint allowed_volume check (volume_of_order >= 1 and volume_of_order <= 1000) not null,
    create_date_order date not null default current_date -- формат даты ГГГГ-ММ-ДД, по умолчанию проставляется текущая дата
);

-- добавим заказы в таблицу orders
-- Иван купил 20 роз
insert into orders(customer_id, product_id, volume_of_order)
values (1, 1, 20);

-- Мария купила 50 лилий
insert into orders(customer_id, product_id, volume_of_order)
values (2, 2, 50);

-- Юлий купил 7 ромашек
insert into orders(customer_id, product_id, volume_of_order)
values (3, 3, 7);

-- Павел купил 3 розы
insert into orders(customer_id, product_id, volume_of_order)
values (4, 1, 3);

-- Паввел купил 6 лилий
insert into orders(customer_id, product_id, volume_of_order)
values (4, 2, 6);

-- Павел купил 1000 ромашек
insert into orders(customer_id, product_id, volume_of_order)
values (4, 3, 1000);

-------------------------------------------------------------------

-- Запросы к базе согласно условиям задачи
-- 1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select o.order_id as "№ заказа", p.product_title as "Продукт", o.volume_of_order as "Кол-во",
       c.customer_name as "Имя", c.customer_phone as "Телефон"
from orders o join customers c on o.customer_id = c.customer_id -- присоединим таблицу customers
              join products p on o.product_id = p.product_id    -- присоединим таблицу products
where order_id = 6;                                             -- выборка по идентификатору заказа = 6

-- 2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select o.order_id as "Номер заказа", c.customer_name as "Имя клиента", o.create_date_order as "Дата заказа"
from customers c join orders o on c.customer_id = o.customer_id
where c.customer_id = 4 and o.create_date_order >= now() - interval '1' month -- где выведи данные по идентификатору клиента (4) и за последний месяц(- interval '1' month)
group by o.create_date_order, o.order_id, c.customer_name;

-- 3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select o.order_id as "Номер заказа", p.product_title as "Название продукта",
       o.volume_of_order as "Количество цветов"
from orders o join products p on p.product_id = o.product_id
where volume_of_order = (select max(volume_of_order) from orders); -- где количество заказа (volume_of_order) имеет максимальное значение в таблице orders

-- 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(volume_of_order) as "Cумма монет по всем заказам"
from orders;

/*
 TODO: Работа в SQL с полем DATE (обновление даты)
 update orders set create_date_order = (date '2022-12-30') where order_id = 10;
 */